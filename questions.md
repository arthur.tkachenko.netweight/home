
### How do I find my menu?

Weekly Menus are located on the Weekly Menu page. 
Log in to your account by clicking “Log In” in the upper right corner of the screen. You will be directed to the current menu.
 
Click on the menu name to see the recipe. Or, use the gray buttons at the top of the page to print or email the complete menu and matching grocery list.

---


### When will I get my weekly menu?

Weekly menus and recipes are posted every Friday between noon and 5pm CST.
The current menu is available for print, email, or viewing by choosing "This Week" from the Menu dropdown. When Friday rolls around again, a new menu is posted; at which time it is rotated to the "Last Week" page.

---

### Can I modify my menu plan based on allergies or preferences?

While our plan does not account for specific dietary restrictions or personal preferences in creating the menus, you are able to select the meals from the current week which will work for you and then add menus from our extensive archives. The site will then generate a personalized menu plan and matching grocery list for your week.

---

### How many meals are included in each week’s menu?

Each weekly menu includes 5 complete meals and a weekend recipe. You can also adjust your menu by adding or removing meals to generate a custom menu and matching grocery list. 

---

### Where can I see a Sample Menu?

You can view each of our menu plans by clicking on the links below. 
Classic Family Sample Menu 
Gluten Free Sample Menu
Cooking for Two Sample Menu

---

### Does No More To-Go offer a Free Trial?

Yes!  We happily offer a 14-day free trial for you to use while deciding whether our meal plans are a good fit for you and your family.
You can choose a meal plan and take it for a taste test to decide if it's a good fit. You will have full membership benefits during your trial period and your membership will then begin on day 15 when your first payment is processed. 
No charge will be made to your account if you cancel anytime within the 14 days. Once cancelled you will retain access to the menus and plans for the duration of your free trial period. 
Start a Free Trial Today! 

---

### What makes No More To-Go different from other meal planning services?

We're so glad you asked!
No More To-Go is passionate about meal planning and recipes!  We've have poured our souls into providing an exceptional service that stands out from other meal plans. 
What started out as a simple hobby into 2011, has turned into a trusted resource for more than 20,000 families across North America!
No More To-Go is not only a meal planning service, it's a community and learning platform that supports home cooks! Our team designs simple step-by-step recipe, photographs the meal, provides cooking tips and tricks, and taste tests everything we make on foodies and kids alike to ensure you are receiving superior menus. 
We are busy just like you and understand the value of flexibility in your meal planning choices. Our weekly menus can be scaled up or down and you can swap featured meals with those from our archives. This allows you to create a personalized weekly menu and matching grocery list.
A few of our other handy features:
Mark your family's favorite meals so you can easily rotate them on to future weekly menu. 
Get recipe tips for picky eaters, gluten free, and vegetarian. 
Adjust serving sizes based on who will be joining you for dinner. 
Customize your weekly menu and matching grocery list. 
Email your menu and list to yourself or a family member. 
Grocery shop from your smart phone. 
The result is a menu we think you’ll love, full of our curated and tested seasonal recipes and menus.

---

### Can I switch to another menu plan after I’ve joined?

You bet! Click on the My Account drop down menu at the top of the page and choose My Profile. In the My Subscriptions section, you will see a red button called ‘Update or Switch Menu’. 

---

### Will I still have access to my menus and recipes when my subscription expires?

No. Our service is subscription based which means you have access to our menus, recipes, lists, and all other member features while you are an active subscriber. Once your subscription expires, you will no longer have access to member content.
For this reason, we encourage you to print or save the menus and lists you'd like to keep prior to terminating your subscription. If you decide to renew in the future, all your saved recipes will be here waiting for you.
Please note: Keeping weekly menu emails that link to the site will not save those menus to your computer. 

---

## How do I save my menus?

You can print the recipes and menus a couple of ways.
Print a single daily menu, click on the name of the menu on the Weekly Menu page, then click the orange Print button. You can also print or save the entire week’s menus and matching grocery list from the Weekly Menu page by clicking on the gray Print button.
Email yourself the complete weekly menu and list with the gray Email button.
The Archives page contains PDF versions of each weekly menu and matching list; simply click on the date of the menu you’d like to print.


---


