
## How it Works...
1
Choose a Weekly Menu Plan
We put an exceptional amount of effort into each and every menu in our meal plans. Every recipe is prepared and taste tested by foodies and picky eaters alike to guarantee your dinner is delicious every night.


### Classic Family Plan


### Gluten Free Family


### Cooking for Two



2
View and Customize Your Weekly Menu


### Customize Your Menu
We design a weekly menu with five complete meals to make dinner exciting. You can mix and match to customize the menu to your family's tastes.

### Substitute a Meal

Not in the mood for pasta? No problem. With the click of a button you can swap an existing meal for any of over 600 meals in our archives.

### Save & Print Recipes

Mark your favorite recipes and easily add them to your custom menus week after week. Then print your recipes or cook from your computer or tablet.
-------------------

3
Generate a Matching Grocery List & Hit the Store

### One-Click Dynamic Grocery Lists

We design a weekly menu with five exciting new dinners which you can mix and match to customize to your family's tastes and schedule.


### Print, Email or Shop from a Device

Print your grocery list in an easy-to-read format, email it to a family member, export to AmazonFresh or shop directly from your smartphone or tablet.


### Organized by Store Department

Ingredients are organized by grocery store department to get you in and out of the store quickly and on budget. No more running back and forth between aisles.

-------------------


4

Cook Exciting New Dinners



### Simple, Clear Instructions


Our recipes are designed to guide you step-by-step (without fancy cooking terms) in creating delicious restaurant-style meals requiring zero skill or stress.

### Tips & Notes from the Kitchen

Each recipe includes prep steps to make weeknight cooking more enjoyable, tips for picky eaters, meatless substitutions, and gluten free modifications.

### Health and Nutrition Facts

Nutrition Facts are provided for every recipe; including calories, fat, saturated fat, sodium, sugar, carbohydrates, fiber, and protein.

-------------------

-------------------

## How it works

### Choose your meals

Every week, your menu plan arrives in your inbox — complete with five weeknight meals and a coordinating grocery list. You can stick with these meals, or mix and match to create your own personalized menu for the week.


S### hop efficiently
Your weekly menu plan includes shopping list organized by department to get you in and out of the store quickly and on budget.


### Cook simple delicious meals
Our step-by-step recipes will have you making chef quality meals in around 30 minutes every night. No cooking skills required; just simple instructions (with no fancy cooking terms) and tips for getting a great meal on the table.

S### ave your time and your budget
We do the planning so you can spend your time and money on the things you love. AND, take the credit for a delicious home cooked meal the whole family will enjoy.
